import java.util.ArrayList;
import java.util.List;

import customRandom.AbstractRandomGenerator;
import customRandom.BinomialRandomGenerator;
import customRandom.ContinuousUniform;
import customRandom.DiscreteUniform;
import customRandom.ExponentialRandomGenerator;
import customRandom.NormalRandomGenerator;

class Main {
    public static void main(String[] args) {

        List<AbstractRandomGenerator> distributions = new ArrayList<>();

        distributions.add(new BinomialRandomGenerator(100));
        distributions.add(new BinomialRandomGenerator(0.9, 100));
        distributions.add(new DiscreteUniform(100, 1000));
        distributions.add(new ContinuousUniform());
        distributions.add(new ContinuousUniform(-100, -50));
        distributions.add(new NormalRandomGenerator());
        distributions.add(new NormalRandomGenerator(100, 10));
        distributions.add(new ExponentialRandomGenerator());
        distributions.add(new ExponentialRandomGenerator(0.5));

        int size = 10_000;

        CSVGenerator csvGenerator = new CSVGenerator();
        csvGenerator.setGenerators(distributions);
        csvGenerator.makeCSV("distribution_data.csv", size);

    }
}
