import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

import customRandom.AbstractRandomGenerator;

public class CSVGenerator {

    private List<AbstractRandomGenerator> generators;

    public CSVGenerator() {
        this.generators = new ArrayList<>();
    }

    public void setGenerators(List<AbstractRandomGenerator> generators) {
        this.generators.clear();
        this.generators.addAll(generators);
    }

    private String[] convertDoubleListToString(List<Double> doubleList) {
        String[] convertedList = new String[doubleList.size()];
        for (int k = 0; k < doubleList.size(); k++) {
            convertedList[k] = String.valueOf(doubleList.get(k));
        }
        return convertedList;
    }

    private String[] getHeader() {
        String[] generatorNames = new String[generators.size()];
        for (int k = 0; k < generators.size(); k++) {
            generatorNames[k] = generators.get(k).getName();
        }
        return generatorNames;
    }

    public void makeCSV(String filePath, int samples) {
        File file = new File(filePath);
        List<Double> oneRow = new ArrayList<>(generators.size());

        for (int k = 0; k < generators.size(); k++) {
            oneRow.add(0.);
        }

        try (FileWriter outputFile = new FileWriter(file)) {
            CSVWriter writer = new CSVWriter(outputFile);

            // write header
            writer.writeNext(getHeader(), false);

            // write one sample at a time
            for (int sample = 0; sample < samples; sample++) {
                for (int k = 0; k < generators.size(); k++) {
                    oneRow.set(k, generators.get(k).generate());
                }
                writer.writeNext(convertDoubleListToString(oneRow), false);
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
