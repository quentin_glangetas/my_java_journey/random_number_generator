package customRandom;

public abstract class AbstractRandomGenerator {

    String name;

    protected static final long DEFAULT_SEED = LinearCongruentialPNRGEngine.DEFAULT_SEED;
    LinearCongruentialPNRGEngine prngEngine;

    public AbstractRandomGenerator(String name, long seed) {
        this.name = name;
        this.prngEngine = new LinearCongruentialPNRGEngine(seed);
    }

    public AbstractRandomGenerator(String name) {
        this(name, DEFAULT_SEED);
    }

    public void setSeed(long seed) {
        prngEngine.setSeed(seed);
    }

    public void setPseudoRandomGeneratorEngine(LinearCongruentialPNRGEngine prngEngine) {
        this.prngEngine = prngEngine;
    }

    public String getName() {
        return name;
    }

    public abstract double generate();

    public void generateMany(double[] randomGenerators, int n) {
        assert (randomGenerators.length >= n);

        for (int k = 0; k < randomGenerators.length; k++) {
            randomGenerators[k] = generate();
        }
    }

    public void generateMany(double[] randomGenerators) {
        generateMany(randomGenerators, randomGenerators.length);
    }

}
