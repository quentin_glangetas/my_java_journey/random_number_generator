package customRandom;

public class DiscreteUniform extends AbstractRandomGenerator {

    private int lowerBound;
    private int higherBound;

    public DiscreteUniform(int a, int b, long seed) {
        super(String.format("discreteUniform(%d, %d)", a, b), seed);
        lowerBound = a < b ? a : b;
        higherBound = a < b ? b : a;
    }

    public DiscreteUniform(int a, int b) {
        this(a, b, DEFAULT_SEED);
    }

    @Override
    public double generate() {
        double continuousUniform = lowerBound + (higherBound - lowerBound) * prngEngine.generateUniform();
        return Math.floor(continuousUniform);
    }

}
