package customRandom;

public class BinomialRandomGenerator extends AbstractRandomGenerator {

    private static final double DEFAULT_PROBABILITY = 0.5;
    private static final int DEFAULT_SAMPLES = 1;

    private BernoulliRandomGenerator bernoulliRNG;
    private int n;

    public BinomialRandomGenerator(double p, int n, long seed) {
        super(String.format("binomial(%.5f, %d)", p, n), seed);
        this.bernoulliRNG = new BernoulliRandomGenerator(p, seed);
        this.n = n;
    }

    public BinomialRandomGenerator(double p, int n) {
        this(p, n, DEFAULT_SEED);
    }

    public BinomialRandomGenerator(double p) {
        this(p, DEFAULT_SAMPLES, DEFAULT_SEED);
    }

    public BinomialRandomGenerator(int n) {
        this(DEFAULT_PROBABILITY, n, DEFAULT_SEED);
    }

    public BinomialRandomGenerator(long seed) {
        this(DEFAULT_PROBABILITY, DEFAULT_SAMPLES, seed);
    }

    @Override
    public double generate() {
        int S = 0;
        for (int k = 0; k < n; k++) {
            S += bernoulliRNG.generate();
        }
        return S;
    }

}
