package customRandom;

public class ContinuousUniform extends AbstractRandomGenerator {

    private static double DEFAULT_LOWER = 0;
    private static double DEFAULT_HIGHER = 1;
    double lowerBound;
    double higherBound;

    public ContinuousUniform(double a, double b, long seed) {
        super(String.format("continuousUniform(%.5f, %.5f)", a, b), seed);
        lowerBound = a < b - 1e-8 ? a : b;
        higherBound = a < b - 1e-8 ? b : a;
    }

    public ContinuousUniform(double a, double b) {
        this(a, b, DEFAULT_SEED);
    }

    public ContinuousUniform(long seed) {
        this(DEFAULT_LOWER, DEFAULT_HIGHER, seed);
    }

    public ContinuousUniform() {
        this(DEFAULT_LOWER, DEFAULT_HIGHER, DEFAULT_SEED);
    }

    @Override
    public double generate() {
        return lowerBound + (higherBound - lowerBound) * prngEngine.generateUniform();
    }

}
