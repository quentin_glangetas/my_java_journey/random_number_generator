package customRandom;

public class NormalRandomGenerator extends AbstractRandomGenerator {

    private static final double DEFAULT_MEAN = 0;
    private static final double DEFAULT_STD = 1;

    private double m;
    private double sigma;
    private double nextSample = Double.NaN; // Use Box-Muller transform, will have 2 independent samples at each iter

    public NormalRandomGenerator(double m, double sigma, long seed) {
        super(String.format("normal(%.5f, %.5f)", m, sigma), seed);
        assert (Math.abs(sigma) > 1e-8);
        this.m = m;
        this.sigma = sigma;
    }

    public NormalRandomGenerator(double m, double sigma) {
        this(m, sigma, DEFAULT_SEED);
    }

    public NormalRandomGenerator(long seed) {
        this(DEFAULT_MEAN, DEFAULT_STD, seed);
    }

    public NormalRandomGenerator() {
        this(DEFAULT_MEAN, DEFAULT_STD, DEFAULT_SEED);
    }

    public Double generateStandardNormal() {
        if (!Double.isNaN(nextSample)) {
            double temp = nextSample;
            nextSample = Double.NaN;
            return temp;
        }

        double u = prngEngine.generateUniform();
        double v = prngEngine.generateUniform();
        double R = Math.sqrt(-2 * Math.log(u));
        double theta = 2 * Math.PI * v;

        nextSample = R * Math.sin(theta);

        return R * Math.cos(theta);
    }

    @Override
    public double generate() {
        double z = generateStandardNormal();
        return m + sigma * z;
    }
}
