package customRandom;

import java.math.BigInteger;

public class LinearCongruentialPNRGEngine {

    private BigInteger seed;
    private BigInteger modulus;
    private long modulusLong;
    private BigInteger multiplier;
    private BigInteger offset;
    private BigInteger currentIter = null;

    // Use java.Random default values
    static final int WARMUP_ITERATIONS = 5;
    static final long DEFAULT_SEED = 1234567890;
    private static final long DEFAULT_MODULUS = (long) Math.pow(2, 48);
    private static final long DEFAULT_MULTIPLIER = 25_214_903_917L;
    private static final long DEFAULT_OFFSET = 11;
    private boolean isWarmedUp = false;

    public LinearCongruentialPNRGEngine(long seed, long modulus, long multiplier, long offset) {
        this.setSeed(seed);
        this.modulusLong = modulus;
        this.modulus = BigInteger.valueOf(modulus);
        this.multiplier = BigInteger.valueOf(multiplier);
        this.offset = BigInteger.valueOf(offset);
    }

    public LinearCongruentialPNRGEngine(long seed) {
        this(seed, DEFAULT_MODULUS, DEFAULT_MULTIPLIER, DEFAULT_OFFSET);
    }

    public LinearCongruentialPNRGEngine() {
        this(DEFAULT_SEED);
    }

    public void setSeed(long seed) {
        this.seed = BigInteger.valueOf(seed);
    }

    public void resetGenerator() {
        this.currentIter = null;
        this.isWarmedUp = false;
    }

    private void warmup() {
        for (int k = 0; k < WARMUP_ITERATIONS; k++) {
            // generate();
            continue;
        }
        isWarmedUp = true;
    }

    public long generate() {
        if (!isWarmedUp) {
            warmup();
        }

        if (currentIter == null) {
            currentIter = seed;
        }

        currentIter = currentIter.multiply(multiplier).add(offset).mod(modulus);

        return currentIter.longValueExact();
    }

    public double generateUniform() {
        long uniform = generate();
        return (double) uniform / modulusLong;
    }

    public long[] generateMany(int n) {
        long[] randomNumbers = new long[n];

        for (int k = 0; k < n; k++) {
            randomNumbers[k] = generate();
        }

        return randomNumbers;
    }

    public double[] generateManyUniforms(int n) {
        double[] randomNumbers = new double[n];

        for (int k = 0; k < n; k++) {
            randomNumbers[k] = generateUniform();
        }

        return randomNumbers;
    }

}
