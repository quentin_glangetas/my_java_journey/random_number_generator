package customRandom;

public class BernoulliRandomGenerator extends AbstractRandomGenerator {

    private static final double DEFAULT_PROBABILITY = 0.5;
    private double p;

    public BernoulliRandomGenerator(double p, long seed) {
        super(String.format("bernoulli(%.5f)", p), seed);
        assert (p > 0 & p < 1);
        this.p = p;
    }

    public BernoulliRandomGenerator(double p) {
        this(p, DEFAULT_SEED);
    }

    public BernoulliRandomGenerator(long seed) {
        this(DEFAULT_PROBABILITY, seed);
    }

    public BernoulliRandomGenerator() {
        this(DEFAULT_PROBABILITY, DEFAULT_SEED);
    }

    @Override
    public double generate() {
        double u = prngEngine.generateUniform();
        return u < p ? 1 : 0;
    }

}
