package customRandom;

public class ExponentialRandomGenerator extends AbstractRandomGenerator {

    private static final double DEFAULT_LAMBDA = 1;
    private double lambda;

    public ExponentialRandomGenerator(double lambda, long seed) {
        super(String.format("exponential(%.5f)", lambda), seed);
        assert (lambda > 0);
        this.lambda = lambda;
    }

    public ExponentialRandomGenerator(double lambda) {
        this(lambda, DEFAULT_SEED);
    }

    public ExponentialRandomGenerator(long seed) {
        this(DEFAULT_LAMBDA, seed);
    }

    public ExponentialRandomGenerator() {
        this(DEFAULT_LAMBDA, DEFAULT_SEED);
    }

    @Override
    public double generate() {
        double u = prngEngine.generateUniform();
        return -Math.log(1 - u) / lambda;
    }

}
