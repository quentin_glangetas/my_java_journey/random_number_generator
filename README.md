# random_number_generator

This pseudo random number generator code base is my first java project. It is build to reflect traditional random number generators in many different languages. Object-Oriented-Programming is used to make the overall project reusable and extendable in the future.


## Goal

The goal of this project was three-fold:
1. Give a nice start to Java programming. Implement, build, debug and run code.
2. Review my understanding of Pseudo Random Number Generation: both in theory and in practice.
3. Focus in using Object-Oriented-Programming (OOP) to implement a robust and flexible project.

## Implementation

Traditionally, to implement a Pseudo Random Number Generator (PRNG), we first implement a discrete PRNG that samples from 0 to a large positive integer N. 

Once this PRNG is implemented, we can divide this sampling by N. This enable us to sample - for a very large N - continuous in the interval (0, 1]. 

From this continuous sampling of (0, 1], we can infer most, if not all, the usual distributions. This comes from the fact that for a law $\mathcal{L}$, we know that:

$$ F_{\mathcal{L}}^{-1}(U) \sim \mathcal{L}$$

where:
* $F_{\mathcal{L}}$ is the cumulative distribution of $\mathcal{L}$
* $U$ is a uniform distribution of the interval (0, 1]


## Examples

### Binomial distribution

The binomial distribution is discrete. The downside of this method is that we are unable to sample the _minimum_ of the distribution, as our uniform sampling is strictly positive. 

![Alt text](binomial_distribution_example.png)


### Exponential distribution

For the exponential distribution we have $F_{\varepsilon(\lambda)}^{-1}(u) = -ln(1-u) / \lambda$.

![Sampling an exponential distribution](exponential_distribution_example.png)

### Normal distribution

To sample a normal distribution we prefer other methods. Indeed, there's no closed form of $F_{\mathcal{N}(\mu, \sigma)}^{-1}$ as of yet. 

One of these methods is the [Box-Muller transform](https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform), which uses 2 independ uniform distribution of (0, 1] to sample from the standard normal distribution. 

![Sampling the standard normal distribution](normal_distribution_example.png)